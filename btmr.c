#include <stdio.h>
#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>
#include "btmr_io.h"
#include "btmr_format.h"
#include "btmr_maths.h"
#include "btmr_glw.h"

#define BTMR_UPDATE_TIME 25

#define BTMR_MP_ROTATION 10
#define BTMR_MP_POSITION 0.05
#define BTMR_MP_MOVEMUL 1.125
#define BTMR_MP_MOVEMUL_LIMIT 0.00001
#define BTMR_MOVEMUL_TO_NEARPLANE 0.0625

#define BTMR_OVERLAY_OPT_NCD 1

typedef struct {
	// The file data
	void * file;
	// options (flags)
	int opt;
	// Loader
	// The loader thread (non-NULL if still loading)
	SDL_Thread * loaderThread;
	// The loader buffer (Owned by loader while that's alive)
	// Really btmr_gpuvertex_t *
	// USE ATOMIC ACCESSES ONLY
	// This must be set to NULL between loads because it's used as an indicator of completion.
	void * loaderBuffer;
	// The loader vertex count (Owned by loader while that's alive)
	size_t loaderVertexCount;
	// The current node (Do not write while loader is alive)
	btmr_node_t * camNode;
	// The GPU-side buffer object representing the current node
	int gpuBuffer;
	// The amount of vertices in gpuBuffer
	size_t gpuVertexCount;
} btmr_overlay_t;

#define BTMR_OVERLAY_MAX 8
// The file
btmr_overlay_t btmr_overlay[BTMR_OVERLAY_MAX];
int btmr_overlayCount;

// X/Y/Z of camera
btmr_v3_t btmr_camPos;
// Pitch (positive up)
btmr_coord_t btmr_camPitch;
// Yaw (positive left)
btmr_coord_t btmr_camYaw;

btmr_coord_t btmr_camMoveMul = 1;

// key flags
#define BTMR_MP_RL 0x00000001
#define BTMR_MP_RU 0x00000002
#define BTMR_MP_RR 0x00000004
#define BTMR_MP_RD 0x00000008

#define BTMR_MP_PL 0x00000010
#define BTMR_MP_PU 0x00000020
#define BTMR_MP_PR 0x00000040
#define BTMR_MP_PD 0x00000080

#define BTMR_MP_HU 0x00000100
#define BTMR_MP_HD 0x00000200

#define BTMR_MP_AU 0x00000400
#define BTMR_MP_AL 0x00000800
#define BTMR_MP_AD 0x00001000

#define BTMR_MP_YL 0x00002000
#define BTMR_MP_YU 0x00004000
#define BTMR_MP_YR 0x00008000
#define BTMR_MP_YD 0x00010000

#define BTMR_MP_MP 0x00020000
#define BTMR_MP_MM 0x00040000

int btmr_mpFlags = 0;

// gpu vertex stuff
typedef struct {
	float posX;
	float posY;
	float posZ;
	float nrmX;
	float nrmY;
	float nrmZ;
} btmr_gpuvertex_t;

void btmr_fillGPUVertex(btmr_gpuvertex_t * target, const btmr_vertex_t * vertex, const btmr_v3_t * normal) {
	target->posX = vertex->pos.x;
	target->posY = vertex->pos.y;
	target->posZ = vertex->pos.z;
	target->nrmX = normal->x;
	target->nrmY = normal->y;
	target->nrmZ = normal->z;
}

void btmr_lightV4(GLenum light, GLenum pname, float a, float b, float c, float d) {
	float arr[4];
	arr[0] = a;
	arr[1] = b;
	arr[2] = c;
	arr[3] = d;
	glLightfv(light, pname, arr);
}

void btmr_materialV4(GLenum face, GLenum pname, float a, float b, float c, float d) {
	float arr[4];
	arr[0] = a;
	arr[1] = b;
	arr[2] = c;
	arr[3] = d;
	glMaterialfv(face, pname, arr);
}

void btmr_light() {
	// R-
	btmr_lightV4(GL_LIGHT0, GL_POSITION, 1, 0, 0, 0);
	btmr_lightV4(GL_LIGHT0, GL_AMBIENT, 0, 0, 0, 1);
	btmr_lightV4(GL_LIGHT0, GL_DIFFUSE, -1, 0, 0, 1);
	btmr_lightV4(GL_LIGHT0, GL_SPECULAR, 0, 0, 0, 1);
	// R+
	btmr_lightV4(GL_LIGHT1, GL_POSITION, -1, 0, 0, 0);
	btmr_lightV4(GL_LIGHT1, GL_AMBIENT, 0, 0, 0, 1);
	btmr_lightV4(GL_LIGHT1, GL_DIFFUSE, 1, 0, 0, 1);
	btmr_lightV4(GL_LIGHT1, GL_SPECULAR, 0, 0, 0, 1);
	// G-
	btmr_lightV4(GL_LIGHT2, GL_POSITION, 0, 0, 1, 0);
	btmr_lightV4(GL_LIGHT2, GL_AMBIENT, 0, 0, 0, 1);
	btmr_lightV4(GL_LIGHT2, GL_DIFFUSE, 0, -1, 0, 1);
	btmr_lightV4(GL_LIGHT2, GL_SPECULAR, 0, 0, 0, 1);
	// G+
	btmr_lightV4(GL_LIGHT3, GL_POSITION, 0, 0, -1, 0);
	btmr_lightV4(GL_LIGHT3, GL_AMBIENT, 0, 0, 0, 1);
	btmr_lightV4(GL_LIGHT3, GL_DIFFUSE, 0, 1, 0, 1);
	btmr_lightV4(GL_LIGHT3, GL_SPECULAR, 0, 0, 0, 1);
	// B-
	btmr_lightV4(GL_LIGHT4, GL_POSITION, 0, 1, 0, 0);
	btmr_lightV4(GL_LIGHT4, GL_AMBIENT, 0, 0, 0, 1);
	btmr_lightV4(GL_LIGHT4, GL_DIFFUSE, 0, 0, -1, 1);
	btmr_lightV4(GL_LIGHT4, GL_SPECULAR, 0, 0, 0, 1);
	// B+
	btmr_lightV4(GL_LIGHT5, GL_POSITION, 0, -1, 0, 0);
	btmr_lightV4(GL_LIGHT5, GL_AMBIENT, 0, 0, 0, 1);
	btmr_lightV4(GL_LIGHT5, GL_DIFFUSE, 0, 0, 1, 1);
	btmr_lightV4(GL_LIGHT5, GL_SPECULAR, 0, 0, 0, 1);
	// mat
	btmr_materialV4(GL_FRONT_AND_BACK, GL_AMBIENT, 0.5, 0.5, 0.5, 1);
	btmr_materialV4(GL_FRONT_AND_BACK, GL_DIFFUSE, 1, 1, 1, 1);
	btmr_materialV4(GL_FRONT_AND_BACK, GL_SPECULAR, 0, 0, 0, 1);
}

int btmr_loaderThread(void * overlayV) {
	btmr_overlay_t * overlay = (btmr_overlay_t *) overlayV;
	// load new buffer!
	puts("loading area...");
	// accumulate model triangle counts
	overlay->loaderVertexCount = 0;
	for (size_t mdl = 0; mdl < BTMR_MAX_MODELS; mdl++)
		overlay->loaderVertexCount += overlay->camNode->triCounts[mdl] * 3;
	// decide what to do
	size_t coordBufferSize = sizeof(btmr_gpuvertex_t) * overlay->loaderVertexCount;
	// it's required that the buffer exist and be safe to free, so give it *some* size
	if (!coordBufferSize)
		coordBufferSize = sizeof(btmr_gpuvertex_t);
	puts("creating buffer...");
	// create buffer
	btmr_gpuvertex_t * theBuffer = (btmr_gpuvertex_t *) malloc(coordBufferSize);
	if (!theBuffer) {
		puts("couldn't malloc intermediate CPU buffer");
		return 1;
	}
	// fill buffer
	size_t vtx = 0;
	for (size_t mdl = 0; mdl < BTMR_MAX_MODELS; mdl++) {
		printf("%li: %li vertices described @ %li\n", mdl, overlay->camNode->triCounts[mdl], overlay->camNode->tris[mdl]);
		size_t triOfs = overlay->camNode->tris[mdl];
		for (size_t i = 0; i < overlay->camNode->triCounts[mdl]; i++) {
			btmr_triangle_t * vptr = BTMR_AT(btmr_triangle_t, overlay->file, triOfs);
			// printf("%li\n", i);
			btmr_vertex_t * a = BTMR_AT(btmr_vertex_t, overlay->file, vptr->a);
			btmr_vertex_t * b = BTMR_AT(btmr_vertex_t, overlay->file, vptr->b);
			btmr_vertex_t * c = BTMR_AT(btmr_vertex_t, overlay->file, vptr->c);

			btmr_v3_t negativeAPos = a->pos;
			btmr_mulV3F(&negativeAPos, -1);

			btmr_v3_t bOffset = b->pos;
			btmr_addV3V3(&bOffset, &negativeAPos);
			btmr_mulV3F(&bOffset, 1 / btmr_lenV3(&bOffset));

			btmr_v3_t cOffset = c->pos;
			btmr_addV3V3(&cOffset, &negativeAPos);
			btmr_mulV3F(&cOffset, 1 / btmr_lenV3(&cOffset));

			btmr_v3_t normal;
			btmr_crossV3(&normal, &bOffset, &cOffset);

			btmr_fillGPUVertex(theBuffer + vtx, a, &normal);
			vtx++;
			btmr_fillGPUVertex(theBuffer + vtx, b, &normal);
			vtx++;
			btmr_fillGPUVertex(theBuffer + vtx, c, &normal);
			vtx++;

			triOfs = vptr->next;
		}
	}
	puts("done");
	SDL_AtomicSetPtr(&overlay->loaderBuffer, theBuffer);
	return 0;
}

void btmr_drawOverlay(btmr_overlay_t * overlay) {
	// detect loader death
	if (overlay->loaderThread) {
		void * lb = SDL_AtomicGetPtr(&overlay->loaderBuffer);
		if (lb) {
			SDL_WaitThread(overlay->loaderThread, NULL);
			overlay->loaderThread = NULL;
			// the thread is dead, move buffer to GPU memory
			size_t coordBufferSize = sizeof(btmr_gpuvertex_t) * overlay->loaderVertexCount;
			glBindBuffer(GL_ARRAY_BUFFER, overlay->gpuBuffer);
			glBufferData(GL_ARRAY_BUFFER, coordBufferSize, lb, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			free(lb);
			SDL_AtomicSetPtr(&overlay->loaderBuffer, NULL);
			overlay->gpuVertexCount = overlay->loaderVertexCount;
		}
	}
	// *if and only if the loader is dead, detect new nodes*
	if (!overlay->loaderThread) {
		btmr_node_t * newNode = btmr_getNodeAt(overlay->file, &btmr_camPos);
		if (newNode != overlay->camNode) {
			overlay->camNode = newNode;
			overlay->loaderThread = SDL_CreateThread(btmr_loaderThread, "emerge", overlay);
		}
	}
	if (overlay->camNode) {
		// setup DrawArrays stuff
		glBindBuffer(GL_ARRAY_BUFFER, overlay->gpuBuffer);
		glVertexPointer(3, GL_FLOAT, sizeof(btmr_gpuvertex_t), NULL);
		glEnableClientState(GL_VERTEX_ARRAY);
		glNormalPointer(GL_FLOAT, sizeof(btmr_gpuvertex_t), (void *) (sizeof(float) * 3));
		glEnableClientState(GL_NORMAL_ARRAY);
		// draw
		glDrawArrays(GL_TRIANGLES, 0, overlay->gpuVertexCount);
		// cleanup
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
	}
}

void btmr_render(int w, int h) {
	// -- Stage 1: base setup --
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, w, h);
	// -- Stage 2: camera setup --
	glMatrixMode(GL_PROJECTION);
	// this is INTENTIONALLY an unusual projection matrix
	// basically, it "has no far plane", and the near plane is at 0
	// scaling factors can be adjusted if this causes any issues
	GLfloat a = (h / ((float) w));
	GLfloat projectionMatrix[] = {
		// row = input axis
		// column = output axis
		a, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, -1, -1,
		0, 0, 0, btmr_camMoveMul * BTMR_MOVEMUL_TO_NEARPLANE
	};
	glLoadMatrixf(projectionMatrix);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotated(-btmr_camPitch, 1, 0, 0);
	glRotated(-btmr_camYaw, 0, 1, 0);
	glTranslated(-btmr_camPos.x, -btmr_camPos.y, -btmr_camPos.z);
	// -- Stage 3: locate player/draw geometry --
	// setup draw mode for 3D stuff
	glEnable(GL_DEPTH_TEST);
	// setup draw mode for lit stuff
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
	glEnable(GL_LIGHT5);
	btmr_light();
	// draw lit stuff
	for (int i = 0; i < btmr_overlayCount; i++) {
		if (!(btmr_overlay[i].opt & BTMR_OVERLAY_OPT_NCD))
			glClear(GL_DEPTH_BUFFER_BIT);
		btmr_drawOverlay(btmr_overlay + i);
	}
	// teardown lit stuff
	glDisable(GL_LIGHT2);
	glDisable(GL_LIGHT1);
	glDisable(GL_LIGHT0);
	glDisable(GL_LIGHTING);
	// unlit stuff
	glBegin(GL_LINES);
	glVertex3f(0, 0, 0);
	glVertex3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 1, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 1);
	// inverse
	glVertex3f(1, 1, 1);
	glVertex3f(0, 1, 1);
	glVertex3f(1, 1, 1);
	glVertex3f(1, 0, 1);
	glVertex3f(1, 1, 1);
	glVertex3f(1, 1, 0);
	// additional Y pylons
	glVertex3f(1, 0, 0);
	glVertex3f(1, 1, 0);
	glVertex3f(0, 0, 1);
	glVertex3f(0, 1, 1);
	// additional X pylons
	glVertex3f(0, 0, 1);
	glVertex3f(1, 0, 1);
	glVertex3f(0, 1, 0);
	glVertex3f(1, 1, 0);
	// additional Z pylons
	glVertex3f(1, 0, 0);
	glVertex3f(1, 0, 1);
	glVertex3f(0, 1, 0);
	glVertex3f(0, 1, 1);
	glEnd();
	// teardown 3D stuff
	glDisable(GL_DEPTH_TEST);
}

int btmr_keyToFlag(int key) {
	// WASD (fly camera controls) - also JL of IJKL
	if (key == SDLK_w)
		return BTMR_MP_PU;
	if (key == SDLK_s)
		return BTMR_MP_PD;
	if ((key == SDLK_a) || (key == SDLK_j))
		return BTMR_MP_PL;
	if ((key == SDLK_d) || (key == SDLK_l))
		return BTMR_MP_PR;
	// LURD (angular controls)
	if (key == SDLK_UP)
		return BTMR_MP_RU;
	if (key == SDLK_DOWN)
		return BTMR_MP_RD;
	if (key == SDLK_LEFT)
		return BTMR_MP_RL;
	if (key == SDLK_RIGHT)
		return BTMR_MP_RR;
	// IK of IJKL (horizontal plane controls)
	if (key == SDLK_i)
		return BTMR_MP_HU;
	if (key == SDLK_k)
		return BTMR_MP_HD;
	// UOP (fixed pitch controls - up, down, level)
	if (key == SDLK_u)
		return BTMR_MP_AU;
	if (key == SDLK_o)
		return BTMR_MP_AD;
	if (key == SDLK_p)
		return BTMR_MP_AL;
	// TFGH (fixed yaw controls)
	if (key == SDLK_t)
		return BTMR_MP_YU;
	if (key == SDLK_g)
		return BTMR_MP_YD;
	if (key == SDLK_f)
		return BTMR_MP_YL;
	if (key == SDLK_h)
		return BTMR_MP_YR;
	// QE (fly speed controls)
	if (key == SDLK_q)
		return BTMR_MP_MP;
	if (key == SDLK_e)
		return BTMR_MP_MM;
	return 0;
}

void btmr_handleEvent(SDL_Event * ev) {
	if (ev->type == SDL_KEYDOWN) {
		int flag = btmr_keyToFlag(ev->key.keysym.sym);
		if (flag)
			btmr_mpFlags |= flag;
	} else if (ev->type == SDL_KEYUP) {
		int flag = btmr_keyToFlag(ev->key.keysym.sym);
		if (flag)
			btmr_mpFlags &= ~flag;
	}
}

void btmr_movementProcessing() {
	// -- apply LURD
	if (btmr_mpFlags & BTMR_MP_RU)
		btmr_camPitch += BTMR_MP_ROTATION;
	if (btmr_mpFlags & BTMR_MP_RD)
		btmr_camPitch -= BTMR_MP_ROTATION;
	if (btmr_mpFlags & BTMR_MP_RL)
		btmr_camYaw += BTMR_MP_ROTATION;
	if (btmr_mpFlags & BTMR_MP_RR)
		btmr_camYaw -= BTMR_MP_ROTATION;
	// -- apply movemul
	if (btmr_mpFlags & BTMR_MP_MP)
		btmr_camMoveMul *= BTMR_MP_MOVEMUL;
	if (btmr_mpFlags & BTMR_MP_MM)
		btmr_camMoveMul /= BTMR_MP_MOVEMUL;
	if (btmr_camMoveMul < BTMR_MP_MOVEMUL_LIMIT)
		btmr_camMoveMul = BTMR_MP_MOVEMUL_LIMIT;
	// -- apply UOP
	if (btmr_mpFlags & BTMR_MP_AU)
		btmr_camPitch = 90;
	if (btmr_mpFlags & BTMR_MP_AL)
		btmr_camPitch = 0;
	if (btmr_mpFlags & BTMR_MP_AD)
		btmr_camPitch = -90;
	// -- apply TFGH
	if (btmr_mpFlags & BTMR_MP_YU)
		btmr_camYaw = 0;
	if (btmr_mpFlags & BTMR_MP_YD)
		btmr_camYaw = 180;
	if (btmr_mpFlags & BTMR_MP_YL)
		btmr_camYaw = 90;
	if (btmr_mpFlags & BTMR_MP_YR)
		btmr_camYaw = -90;
	// -- convert angles to radians
	btmr_coord_t pitchRad = DEG2RAD(btmr_camPitch);
	btmr_coord_t yawRad = DEG2RAD(btmr_camYaw);
	// -- calc base vectors
	btmr_v2_t yawFwd;
	btmr_v2_t yawRight;
	btmr_v2_t pitchFwd;
	btmr_v2_t pitchUp;
	btmr_v2_t tmp;

	btmr_v2FromAngle(&yawRight, yawRad);
	// pitch X controls horizontal movement, while pitch Y controls vertical movement
	// not a matrix in sight!
	btmr_v2FromAngle(&pitchFwd, pitchRad);
	pitchUp = pitchFwd;
	btmr_rotV290CCW(&pitchUp);
	yawFwd = yawRight;
	btmr_rotV290CCW(&yawFwd);
	// -- calc SWAD
	btmr_coord_t sw = 0;
	btmr_coord_t swHP = 0;
	btmr_coord_t ad = 0;
	if (btmr_mpFlags & BTMR_MP_PU)
		sw++;
	if (btmr_mpFlags & BTMR_MP_PD)
		sw--;
	if (btmr_mpFlags & BTMR_MP_PL)
		ad--;
	if (btmr_mpFlags & BTMR_MP_PR)
		ad++;
	if (btmr_mpFlags & BTMR_MP_HU)
		swHP++;
	if (btmr_mpFlags & BTMR_MP_HD)
		swHP--;
	// -- apply SW
	tmp = yawFwd;
	btmr_mulV2F(&tmp, sw * pitchFwd.x * BTMR_MP_POSITION * btmr_camMoveMul);
	btmr_camPos.y -= sw * pitchFwd.y * BTMR_MP_POSITION * btmr_camMoveMul;
	btmr_addV2V3XZ(&btmr_camPos, &tmp);
	// -- apply SW (horizontal plane)
	tmp = yawFwd;
	btmr_mulV2F(&tmp, swHP * pitchUp.x * BTMR_MP_POSITION * btmr_camMoveMul);
	btmr_camPos.y -= swHP * pitchUp.y * BTMR_MP_POSITION * btmr_camMoveMul;
	btmr_addV2V3XZ(&btmr_camPos, &tmp);
	// -- apply AD
	tmp = yawRight;
	btmr_mulV2F(&tmp, ad * BTMR_MP_POSITION * btmr_camMoveMul);
	btmr_addV2V3XZ(&btmr_camPos, &tmp);
}

int main(int argc, char ** argv) {
	if (argc == 1) {
		puts("btmr FILE...");
		puts("FILEs must be BTMR files from btmr_import/etc.");
		puts("'=' at the start of a file *disables* clearing depth buffer between files");
		return 1;
	}
	if (argc - 1 > BTMR_OVERLAY_MAX) {
		puts("too many overlays");
		return 1;
	}
	puts("mapping overlays...");
	for (int i = 1; i < argc; i++) {
		const char * arg = argv[i];
		int opt = 0;
		if (*arg == '=') {
			opt |= BTMR_OVERLAY_OPT_NCD;
			arg++;
		}
		puts(arg);
		void * fm = btmr_mapDumb(arg);
		if (!fm) {
			puts("unable to map file");
			return 1;
		}
		btmr_overlay[i - 1].file = fm;
		btmr_overlay[i - 1].opt = opt;
		btmr_overlayCount++;
	}
	puts("starting video...");
	// -- done with init --
	// just assume a ton of stuff succeeds for now
	if (SDL_Init(SDL_INIT_VIDEO)) {
		puts("unable to init SDL");
		return 1;
	}
	SDL_Window * window = SDL_CreateWindow("Big Triangle Mesh Renderer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
	if (!window) {
		puts("unable to create window");
		return 1;
	}
	if (!SDL_GL_CreateContext(window)) {
		puts("unable to create/set GL context");
		return 1;
	}
	// Pre-init face culling
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	// Pre-init buffer object creation
	for (int i = 0; i < btmr_overlayCount; i++)
		glGenBuffers(1, &btmr_overlay[i].gpuBuffer);
	Uint32 lastUpdate = 0;
	while (1) {
		// TODO: UNBREAK SCHEDULING
		SDL_Event ev;
		if (SDL_WaitEventTimeout(&ev, BTMR_UPDATE_TIME)) {
			// event ; handle it
			if (ev.type == SDL_QUIT) {
				return 0;
			} else {
				btmr_handleEvent(&ev);
			}
		}
		// Prevent too-frequent updates
		Uint32 now = SDL_GetTicks();
		if (now >= (lastUpdate + BTMR_UPDATE_TIME)) {
			btmr_movementProcessing();
			// Actually update display
			int w, h;
			SDL_GL_GetDrawableSize(window, &w, &h);
			btmr_render(w, h);
			SDL_GL_SwapWindow(window);
			// Mark the update as complete
			lastUpdate = now;
		}
	}
}
