#include "btmr_maths.h"
#include <math.h>
#include <SDL.h>
#include <stdio.h>

// V2

void btmr_v2FromAngle(btmr_v2_t * target, btmr_coord_t angle) {
	target->x = SDL_cos(angle);
	target->y = -SDL_sin(angle);
}

void btmr_rotV290CCW(btmr_v2_t * target) {
	btmr_coord_t tmp = target->x;
	target->x = target->y;
	target->y = -tmp;
}

void btmr_rotV290CW(btmr_v2_t * target) {
	btmr_coord_t tmp = target->x;
	target->x = -target->y;
	target->y = tmp;
}

// V3

btmr_coord_t btmr_dotV3V3(const btmr_v3_t * target, const btmr_v3_t * source) {
	btmr_v3_t tmp = *target;
	btmr_mulV3V3(&tmp, source);
	return tmp.x + tmp.y + tmp.z;
}

btmr_coord_t btmr_lenV3(const btmr_v3_t * source) {
	return SDL_sqrt((source->x * source->x) + (source->y * source->y) + (source->z * source->z));
}

void btmr_crossV3(btmr_v3_t * target, const btmr_v3_t * b, const btmr_v3_t * c) {
	target->x = (b->y * c->z) - (b->z * c->y);
	target->y = (b->z * c->x) - (b->x * c->z);
	target->z = (b->x * c->y) - (b->y * c->x);
}

// combined

void btmr_addV2V3XZ(btmr_v3_t * target, const btmr_v2_t * source) {
	target->x += source->x;
	target->z += source->y;
}

btmr_coord_t btmr_aboveNodePlaneDist(const btmr_node_t * node, const btmr_v3_t * point) {
	if (node->planeAxis == 0) {
		return point->x - node->planePos;
	} else if (node->planeAxis == 1) {
		return point->y - node->planePos;
	} else {
		return point->z - node->planePos;
	}
}

btmr_node_t * btmr_getNodeAt(void * file, const btmr_v3_t * point) {
	btmr_node_t * base = file;
	while (base->a)
		base = BTMR_AT(btmr_node_t, file, (btmr_aboveNodePlaneDist(base, point) >= 0) ? base->a : base->b);
	return base;
}

// BB3

const btmr_bb3_t btmr_bb3Infinity = {
	.min = {
		.x = -INFINITY,
		.y = -INFINITY,
		.z = -INFINITY
	},
	.max = {
		.x = INFINITY,
		.y = INFINITY,
		.z = INFINITY
	}
};

#define BTMR_MAXORMINB(a1, a2) (b ? SDL_min(a1, a2) : SDL_max(a1, a2))
void btmr_clipBB3ByNode(btmr_bb3_t * box, const btmr_node_t * node, int b) {
	// before continuing:
	// BTMR_MAXORMINB uses min if b == 1 (i.e. we're on the below side, the negative side)
	//  and uses max otherwise (i.e. we're on the above side, the positive side)
	if (node->planeAxis == 0) {
		// X
		box->min.x = BTMR_MAXORMINB(box->min.x, node->planePos);
		box->max.x = BTMR_MAXORMINB(box->max.x, node->planePos);
	} else if (node->planeAxis == 1) {
		// Y
		box->min.y = BTMR_MAXORMINB(box->min.y, node->planePos);
		box->max.y = BTMR_MAXORMINB(box->max.y, node->planePos);
	} else if (node->planeAxis == 2) {
		// Z
		box->min.z = BTMR_MAXORMINB(box->min.z, node->planePos);
		box->max.z = BTMR_MAXORMINB(box->max.z, node->planePos);
	}
}
#undef BTMR_MAXORMINB

#define BTMR_AELM_TOUCHING(rng, elm, axis) ((elm >= rng->min.axis) && (elm <= rng->max.axis))
#define BTMR_ARNG_TOUCHING(efa, efb, axis) (BTMR_AELM_TOUCHING(efb, efa->min.axis, axis) || BTMR_AELM_TOUCHING(efb, efa->max.axis, axis))
#define BTMR_AXIS_TOUCHING(axis) (BTMR_ARNG_TOUCHING(a, b, axis) || BTMR_ARNG_TOUCHING(b, a, axis))
int btmr_bb3Neighbour(const btmr_bb3_t * a, const btmr_bb3_t * b) {
	return BTMR_AXIS_TOUCHING(x) && BTMR_AXIS_TOUCHING(y) && BTMR_AXIS_TOUCHING(z);
}
#undef BTMR_AXIS_TOUCHING

// Printers

void btmr_printV3(const btmr_v3_t * a) {
	printf("(%f %f %f)", a->x, a->y, a->z);
}
void btmr_printBB3(const btmr_bb3_t * a) {
	printf("(");
	btmr_printV3(&a->min);
	printf(" - ");
	btmr_printV3(&a->max);
	printf(")");
}

