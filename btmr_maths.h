#pragma once

#include "btmr_format.h"

#define DEG2RAD(d) ((d) * (3.1415926535898 / 180.0))

// types

typedef struct {
	btmr_coord_t x;
	btmr_coord_t y;
} btmr_v2_t;

typedef struct {
	btmr_v3_t min;
	btmr_v3_t max;
} btmr_bb3_t;

// universal vector/vector add

static inline void btmr_addV2V2(btmr_v2_t * target, const btmr_v2_t * source) {
	target->x += source->x;
	target->y += source->y;
}
static inline void btmr_addV3V3(btmr_v3_t * target, const btmr_v3_t * source) {
	target->x += source->x;
	target->y += source->y;
	target->z += source->z;
}

// universal vector/vector mul

static inline void btmr_mulV2V2(btmr_v2_t * target, const btmr_v2_t * source) {
	target->x *= source->x;
	target->y *= source->y;
}
static inline void btmr_mulV3V3(btmr_v3_t * target, const btmr_v3_t * source) {
	target->x *= source->x;
	target->y *= source->y;
	target->z *= source->z;
}

// universal vector/float mul

static inline void btmr_mulV2F(btmr_v2_t * target, btmr_coord_t source) {
	target->x *= source;
	target->y *= source;
}
static inline void btmr_mulV3F(btmr_v3_t * target, btmr_coord_t source) {
	target->x *= source;
	target->y *= source;
	target->z *= source;
}

// V2

// V2 functions all work in a consistent space:
// up is -Y
// angle of 0 radians is right
// angle of pi/2 radians is down

void btmr_v2FromAngle(btmr_v2_t * target, btmr_coord_t angle);

void btmr_rotV290CCW(btmr_v2_t * target);
void btmr_rotV290CW(btmr_v2_t * target);

// V3

btmr_coord_t btmr_dotV3V3(const btmr_v3_t * target, const btmr_v3_t * source);

btmr_coord_t btmr_lenV3(const btmr_v3_t * source);

void btmr_crossV3(btmr_v3_t * target, const btmr_v3_t * b, const btmr_v3_t * c);

// combined

void btmr_addV2V3XZ(btmr_v3_t * target, const btmr_v2_t * source);

btmr_coord_t btmr_aboveNodePlaneDist(const btmr_node_t * node, const btmr_v3_t * point);

btmr_node_t * btmr_getNodeAt(void * file, const btmr_v3_t * point);

// BB3

extern const btmr_bb3_t btmr_bb3Infinity;

void btmr_clipBB3ByNode(btmr_bb3_t * box, const btmr_node_t * node, int b);

// The rules for this are:
// Colliding: 1
// Surfaces touching: 1
// Points touching: 1
// None of the above: 0
int btmr_bb3Neighbour(const btmr_bb3_t * a, const btmr_bb3_t * b);

// Printers

void btmr_printV3(const btmr_v3_t * a);
void btmr_printBB3(const btmr_bb3_t * a);

