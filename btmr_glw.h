#pragma once

#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>

static PFNGLGENBUFFERSPROC btmr_glGenBuffers = NULL;
static PFNGLBINDBUFFERPROC btmr_glBindBuffer = NULL;
static PFNGLBUFFERDATAPROC btmr_glBufferData = NULL;

void glGenBuffers(GLsizei a0, GLuint * a1) {
	if (!btmr_glGenBuffers)
		btmr_glGenBuffers = SDL_GL_GetProcAddress("glGenBuffers");
	btmr_glGenBuffers(a0, a1);
}

void glBindBuffer(GLenum a0, GLuint a1) {
	if (!btmr_glBindBuffer)
		btmr_glBindBuffer = SDL_GL_GetProcAddress("glBindBuffer");
	btmr_glBindBuffer(a0, a1);
}

void glBufferData(GLenum a0, GLsizeiptr a1, const void * a2, GLenum a3) {
	if (!btmr_glBufferData)
		btmr_glBufferData = SDL_GL_GetProcAddress("glBufferData");
	btmr_glBufferData(a0, a1, a2, a3);
}

