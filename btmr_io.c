#ifndef WINDOWS
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#else
#include <windows.h>
#endif

#include <stdlib.h>
#include <stdio.h>

// this bit is a bit special, because of the private (OS-specific!) struct

typedef struct {
	void * memory;
	size_t size;
#ifndef WINDOWS
	int fd;
#else
	HANDLE file;
	// WHY DO THESE EXIST. Who thought we needed a dedicated object for this?
	// It's stupid. Thank you Windows for making me hate you all over again.
	HANDLE fileMapping;
#endif
	int rw;
	void * fixups;
} btmr_mapping_dt, * btmr_mapping_pt;

#define btmr_mapping_t btmr_mapping_pt
#include "btmr_io.h"

void * btmr_mapDumb(const char * fn) {
	btmr_mapping_t map = btmr_mappingOpen(fn, 0);
	if (!map)
		return NULL;
	return btmr_mappingGetMemory(map);
}

// the clever API

// used to perform the actual work of setting up size / memory
static btmr_mapping_t btmr_mappingCore(btmr_mapping_t map) {
	void * oldMemory = map->memory;
#ifndef WINDOWS
	map->size = lseek(map->fd, 0, SEEK_END);
	lseek(map->fd, 0, SEEK_SET);
	// note the usage of oldMemory as a mapping hint
	// to try and prevent having to use fixups
	map->memory = mmap(oldMemory, map->size, map->rw ? (PROT_READ | PROT_WRITE) : PROT_READ, MAP_SHARED, map->fd, 0);
	if (map->memory == MAP_FAILED) {
		close(map->fd);
		free(map);
		return NULL;
	}
#else
	LARGE_INTEGER tmpFileSize;
	GetFileSizeEx(map->file, &tmpFileSize);
	map->size = (size_t) tmpFileSize.QuadPart;
	map->fileMapping = CreateFileMappingA(map->file, NULL, PAGE_READWRITE, 0, 0, NULL);
	// guess you'll have to use fixups then
	map->memory = MapViewOfFile(map->fileMapping, FILE_MAP_ALL_ACCESS, 0, 0, 0);
	if (!map->memory) {
		CloseHandle(map->fileMapping);
		CloseHandle(map->file);
		free(map);
		return NULL;
	}
#endif
	// fixups
	intptr_t offset = ((intptr_t) map->memory) - ((intptr_t) oldMemory);
	if (offset) {
		btmr_fixup_t * leaf = map->fixups;
		while (leaf) {
			*leaf->target += offset;
			leaf = leaf->parent;
		}
	}
	// printf("%lu bytes mapped into memory\n", map->length);
	return map;
}

// Opens a file for reading or reading/writing and maps it into memory.
// The file may be unmapped.
btmr_mapping_t btmr_mappingOpen(const char * fn, int rw) {
	btmr_mapping_t map = (btmr_mapping_t) malloc(sizeof(btmr_mapping_dt));
	if (!map) {
		puts("wow, failed to malloc a management struct");
		return NULL;
	}
#ifndef WINDOWS
	map->fd = open(fn, rw ? O_RDWR : O_RDONLY);
	if (map->fd < 0) {
		puts("couldn't open");
		free(map);
		return NULL;
	}
#else
	map->file = CreateFileA(fn, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (!map->file) {
		puts("couldn't open");
		free(map);
		return NULL;
	}
#endif
	map->rw = rw;
	map->fixups = NULL;
	map->memory = NULL;
	return btmr_mappingCore(map);
}

// Gets the mapped location of the file.
void * btmr_mappingGetMemory(btmr_mapping_t map) {
	return map->memory;
}

// Gets the size of the file.
size_t btmr_mappingGetSize(btmr_mapping_t map) {
	return map->size;
}

// Resizes the file (padding with zeroes).
// This may cause the mapping to be moved - the new location is returned.
// (This attribute is primarily paranoia in case Windows porting requires it.)
// If NULL is returned, resizing the file failed and the mapping was closed as a result.
void * btmr_mappingSetSize(btmr_mapping_t map, size_t newSize) {
#ifndef WINDOWS
	munmap(map->memory, map->size);
	if (ftruncate(map->fd, newSize)) {
		puts("unable to ftruncate, panic!");
		exit(1);
	}
#else
	UnmapViewOfFile(map->memory);
	CloseHandle(map->fileMapping);
	LARGE_INTEGER newSizeAsLI;
	newSizeAsLI.QuadPart = newSize;
	SetFilePointerEx(map->file, newSizeAsLI, NULL, FILE_BEGIN);
	SetEndOfFile(map->file);
	SetFilePointer(map->file, 0, NULL, FILE_BEGIN);
#endif
	map = btmr_mappingCore(map);
	if (!map)
		return NULL;
	return map->memory;
}

void btmr_mappingClose(btmr_mapping_t map) {
#ifndef WINDOWS
	munmap(map->memory, map->size);
	close(map->fd);
#else
	UnmapViewOfFile(map->memory);
	CloseHandle(map->fileMapping);
	CloseHandle(map->file);
#endif
	free(map);
}

// -- Fixups --

void btmr_fixupAdd(btmr_mapping_t map, btmr_fixup_t * add, void ** fixup) {
	add->parent = map->fixups;
	add->target = fixup;
	map->fixups = add;
}

void btmr_fixupRm(btmr_mapping_t map, void ** fixup) {
	if (((btmr_fixup_t *) map->fixups)->target != fixup) {
		puts("corruption on fixup stack");
		exit(1);
	}
	map->fixups = ((btmr_fixup_t *) map->fixups)->parent;
}

// -- Alloc --

void * btmr_mappingAlloc(btmr_mapping_t map, size_t len) {
	size_t oldSize = btmr_mappingGetSize(map);
	void * oldFile = btmr_mappingGetMemory(map);
	void * endOfOldFile = oldFile + oldSize;
	BTMR_FIXUP_ENTER(map, endOfOldFile);
	if (!btmr_mappingSetSize(map, oldSize + len)) {
		puts("file allocation failed");
		exit(1);
	}
	BTMR_FIXUP_LEAVE(map, endOfOldFile);
	return endOfOldFile;
}

