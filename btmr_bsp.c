#include <stdio.h>
#include <string.h>
#include <SDL.h>

#include "btmr_format.h"
#include "btmr_maths.h"
#include "btmr_io.h"

btmr_mapping_t btmr_fileMap;
void * btmr_file;

void * btmr_optAlloc(size_t len) {
	return btmr_mappingAlloc(btmr_fileMap, len);
}

#define BTMR_OPT_ALLOC(type) ((type *)(btmr_optAlloc(sizeof(type))))

// ----

// debugging opts.
#define BTMR_SPLIT_DEBUG_LOW
// #define BTMR_SPLIT_DEBUG

// ----

size_t btmr_budget;

int btmr_bspNode(btmr_node_t * node) {
	int splits = 0;
	BTMR_FIXUP_ENTER(btmr_fileMap, node);
	// Before we begin, nuke alternate models
	for (size_t i = 1; i < BTMR_MAX_MODELS; i++)
		node->triCounts[i] = 0;
	// Continue...
	if (!node->a) {
		// Not a split node - check if this needs to be split.
		if (node->triCounts[0] > btmr_budget) {
#ifdef BTMR_SPLIT_DEBUG_LOW
			printf("%lu triangles in node - splitting\n", node->triCounts[0]);
#endif
			// find extents of the node
			btmr_bb3_t extents;
			size_t triOfs = node->tris[0];
			for (size_t i = 0; i < node->triCounts[0]; i++) {
				btmr_triangle_t * vptrs = BTMR_AT(btmr_triangle_t, btmr_file, triOfs);
				btmr_vertex_t * vtx = BTMR_AT(btmr_vertex_t, btmr_file, vptrs->a);
				if (i == 0) {
					extents.min.x = extents.max.x = vtx->pos.x;
					extents.min.y = extents.max.y = vtx->pos.y;
					extents.min.z = extents.max.z = vtx->pos.z;
				} else {
					extents.min.x = SDL_min(extents.min.x, vtx->pos.x);
					extents.min.y = SDL_min(extents.min.y, vtx->pos.y);
					extents.min.z = SDL_min(extents.min.z, vtx->pos.z);
					extents.max.x = SDL_max(extents.max.x, vtx->pos.x);
					extents.max.y = SDL_max(extents.max.y, vtx->pos.y);
					extents.max.z = SDL_max(extents.max.z, vtx->pos.z);
				}
				// advance!
				triOfs = vptrs->next;
			}
			// use biggest extent for axis
			btmr_v3_t extentSizeFudge;
			extentSizeFudge.x = extents.max.x - extents.min.x;
			extentSizeFudge.y = extents.max.y - extents.min.y;
			extentSizeFudge.z = extents.max.z - extents.min.z;
			// here's the "fudge" part; reduce likelihood of Y splits
			// this tends to massively decrease the amount of neighbours
			// disabled for use with sponge!!!
			// extentSizeFudge.y *= 0.125;
			// actually find biggest extent
			if (extentSizeFudge.x > extentSizeFudge.y) {
				if (extentSizeFudge.x > extentSizeFudge.z) {
					node->planeAxis = 0;
				} else {
					node->planeAxis = 2;
				}
			} else {
				if (extentSizeFudge.y > extentSizeFudge.z) {
					node->planeAxis = 1;
				} else {
					node->planeAxis = 2;
				}
			}
#ifdef BTMR_SPLIT_DEBUG_LOW
			printf(" axis %i (extents ", node->planeAxis);
			btmr_printBB3(&extents);
			printf(")\n");
#endif
			// use those distance figures to get a plane position
			if (node->planeAxis == 0)
				node->planePos = (extents.min.x + extents.max.x) / 2;
			if (node->planeAxis == 1)
				node->planePos = (extents.min.y + extents.max.y) / 2;
			if (node->planeAxis == 2)
				node->planePos = (extents.min.z + extents.max.z) / 2;
			// BSP split plane definition complete. Create nodes A and B.
			btmr_node_t * subNodeA = BTMR_OPT_ALLOC(btmr_node_t);
			memset(subNodeA, 0, sizeof(btmr_node_t));
			// create fixup because of next allocation
			BTMR_FIXUP_ENTER(btmr_fileMap, subNodeA);
			btmr_node_t * subNodeB = BTMR_OPT_ALLOC(btmr_node_t);
			memset(subNodeB, 0, sizeof(btmr_node_t));
			// okie-dokie!
#ifdef BTMR_SPLIT_DEBUG
			puts("split started");
#endif
			// fill them. This is an in-place operation which never allocates.
			// go through each triangle...
			triOfs = node->tris[0];
			for (size_t i = 0; i < node->triCounts[0]; i++) {
				btmr_triangle_t * vptrs = BTMR_AT(btmr_triangle_t, btmr_file, triOfs);
				// 'advance' early because the links will be changed around
				triOfs = vptrs->next;
				// continue...
				btmr_vertex_t * vtx = BTMR_AT(btmr_vertex_t, btmr_file, vptrs->a);
				// find target subNode
				btmr_node_t * targetNode = subNodeB;
				if (btmr_aboveNodePlaneDist(node, &vtx->pos) >= 0)
					targetNode = subNodeA;
				// link it in (in reverse)
				vptrs->next = targetNode->tris[0];
				targetNode->tris[0] = BTMR_OFS(btmr_file, vptrs);
				targetNode->triCounts[0]++;
			}
			// all triangles were consumed, so zero out the parent node's triangle count.
			node->triCounts[0] = 0;
			// cleanup.
			node->a = BTMR_OFS(btmr_file, subNodeA);
			node->b = BTMR_OFS(btmr_file, subNodeB);
			BTMR_FIXUP_LEAVE(btmr_fileMap, subNodeA);
			splits++;
#ifdef BTMR_SPLIT_DEBUG
			puts("completed split");
#endif
		}
	}
	// done splitting (if necessary), check sub-nodes?
	if (node->a)
		splits += btmr_bspNode(BTMR_AT(btmr_node_t, btmr_file, node->a));
	if (node->b)
		splits += btmr_bspNode(BTMR_AT(btmr_node_t, btmr_file, node->b));
	BTMR_FIXUP_LEAVE(btmr_fileMap, node);
	return splits;
}

int main(int argc, char ** argv) {
	if (argc != 3) {
		puts("btmr_bsp TARGET BUDGET");
		puts("WARNING: DO NOT PUT UNTRUSTED FILES INTO THIS THING.");
		puts("IT PERFORMS WRITES THAT COULD GO OUT OF BOUNDS. YOU'VE BEEN WARNED.");
		puts("be virgilent.");
		return 1;
	}
	btmr_budget = atoi(argv[2]);
	btmr_fileMap = btmr_mappingOpen(argv[1], 1);
	if (!btmr_fileMap) {
		puts("couldn't open mapping");
		return 1;
	}
	btmr_file = btmr_mappingGetMemory(btmr_fileMap);
	BTMR_FIXUP_ENTER(btmr_fileMap, btmr_file);
	// bsp/lod
	int splits = btmr_bspNode((btmr_node_t *) btmr_file);
	if (splits == 1) {
		printf("%i split performed\n", splits);
	} else {
		printf("%i splits performed\n", splits);
	}
	BTMR_FIXUP_LEAVE(btmr_fileMap, btmr_file);
	btmr_mappingClose(btmr_fileMap);
	return 0;
}

