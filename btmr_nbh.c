#include <stdio.h>

#include "btmr_format.h"
#include "btmr_maths.h"
#include "btmr_io.h"

void * btmr_file;

int btmr_nbhSuccessful;
int btmr_nbhFailed;

size_t btmr_maxHCount;

typedef void (*btmr_nbhScanFunc_t)(btmr_node_t * node, const btmr_bb3_t * aabb, void * userdata);

int btmr_scanWithAABBs(btmr_node_t * node, const btmr_bb3_t * base, btmr_nbhScanFunc_t fn, void * userdata) {
	if (node->a) {
		int leafCount = 0;
		btmr_bb3_t tmp = *base;
		btmr_clipBB3ByNode(&tmp, node, 0);
		leafCount += btmr_scanWithAABBs(BTMR_AT(btmr_node_t, btmr_file, node->a), &tmp, fn, userdata);
		tmp = *base;
		btmr_clipBB3ByNode(&tmp, node, 1);
		leafCount += btmr_scanWithAABBs(BTMR_AT(btmr_node_t, btmr_file, node->b), &tmp, fn, userdata);
		return leafCount;
	} else {
		fn(node, base, userdata);
		return 1;
	}
}

typedef struct {
	btmr_node_t * node;
	const btmr_bb3_t * base;
	// This is the amount of model slots that are in use
	size_t count;
	// This is like count, but it also goes up even if we're out of model slots
	// This is used to count placements that we "should've" done
	size_t hCount;
} btmr_nbhIUD;

void btmr_nbh2(btmr_node_t * node, const btmr_bb3_t * base, void * userdata) {
	btmr_nbhIUD * iud = (btmr_nbhIUD *) userdata;
	if (iud->node == node)
		return;
	// puts("placement check");
	// btmr_printBB3(iud->base);
	// btmr_printBB3(base);
	if (btmr_bb3Neighbour(iud->base, base)) {
		// printf("yup\n");
		iud->hCount++;
		if (iud->count == BTMR_MAX_MODELS) {
			btmr_nbhFailed++;
		} else {
			// copy the model header
			iud->node->tris[iud->count] = node->tris[0];
			iud->node->triCounts[iud->count] = node->triCounts[0];
			iud->count++;
			btmr_nbhSuccessful++;
		}
	}
}

void btmr_nbh(btmr_node_t * node, const btmr_bb3_t * base, void * userdata) {
	// wipe all other models
	for (size_t i = 1; i < BTMR_MAX_MODELS; i++)
		node->triCounts[i] = 0;
	// scan for neighbours
	btmr_nbhIUD subData;
	subData.node = node;
	subData.base = base;
	// this is 1 because model 0 is, of course, the primary model
	subData.count = 1;
	subData.hCount = 1;
	btmr_scanWithAABBs((btmr_node_t *) btmr_file, &btmr_bb3Infinity, btmr_nbh2, &subData);
	if (subData.hCount > btmr_maxHCount) {
		btmr_maxHCount = subData.hCount;
	}
}

int main(int argc, char ** argv) {
	if (argc != 2) {
		puts("btmr_nbh BTMRFILE");
		return 1;
	}
	btmr_mapping_t fileMap = btmr_mappingOpen(argv[1], 1);
	if (!fileMap) {
		puts("couldn't open mapping");
		return 1;
	}
	btmr_file = btmr_mappingGetMemory(fileMap);
	int leafCount = btmr_scanWithAABBs((btmr_node_t *) btmr_file, &btmr_bb3Infinity, btmr_nbh, NULL);
	printf("Leaves: %i\nSuccessful placements: %i\nFailed placements (out of model slots): %i\nModel slot usage: %i / %i\n", leafCount, btmr_nbhSuccessful, btmr_nbhFailed, (int) btmr_maxHCount, BTMR_MAX_MODELS);
	btmr_mappingClose(fileMap);
	return 0;
}

