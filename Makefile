LIB_SOURCES := btmr_io.c btmr_maths.c
LIB_HEADERS := btmr_io.h btmr_format.h btmr_maths.h btmr_glw.h

U_CC := gcc
U_CFLAGS := -flto -O4 -I/usr/include/SDL2
U_LDFLAGS := -lSDL2 -lGL

W_CC := x86_64-w64-mingw32-gcc
W_CFLAGS := -flto -O4 -I/usr/include/SDL2 -DWINDOWS -DSDL_MAIN_HANDLED
W_LDFLAGS := -Lbin -lSDL2 -lopengl32

BINARIES := bin/btmr bin/btmr_bsp bin/btmr_nbh bin/btmr_import bin/btmr_genMenger
ifeq ($(NO_MINGW),)
BINARIES += bin/btmr.exe bin/btmr_bsp.exe bin/btmr_nbh.exe bin/btmr_import.exe bin/btmr_genMenger.exe
endif

all: $(BINARIES)

clean:
	rm -f $(BINARIES)

bin/%: %.c $(LIB_SOURCES) $(LIB_HEADERS)
	$(U_CC) $(U_CFLAGS) -o $@ $< $(LIB_SOURCES) $(U_LDFLAGS)

bin/%.exe: %.c $(LIB_SOURCES) $(LIB_HEADERS)
	$(W_CC) $(W_CFLAGS) -o $@ $< $(LIB_SOURCES) $(W_LDFLAGS)
