#include <stdio.h>
#include "btmr_maths.h"

// vertices
size_t btmr_vtxId = 1;
size_t btmr_genVtx(const btmr_v3_t * pos) {
	printf("v %f %f %f\n", (float) pos->x, (float) pos->y, (float) pos->z);
	return btmr_vtxId++;
}

// cubefaces
#define BTMR_CF_PX 0x01
#define BTMR_CF_PY 0x02
#define BTMR_CF_PZ 0x04
#define BTMR_CF_MX 0x08
#define BTMR_CF_MY 0x10
#define BTMR_CF_MZ 0x20
#define BTMR_CF_FF 0x3F

#define BTMR_QUAD_FMT "f %lu %lu %lu %lu\n"

void btmr_genCube(const btmr_v3_t * pos, btmr_coord_t size, int flags) {
	// ABX
	// CD
	// Z

	btmr_v3_t al, bl, cl, dl;
	al = *pos;
	bl = *pos; bl.x += size;
	cl = *pos; cl.z += size;
	dl = *pos; dl.x += size; dl.z += size;

	btmr_v3_t ah, bh, ch, dh;
	ah = al; ah.y += size;
	bh = bl; bh.y += size;
	ch = cl; ch.y += size;
	dh = dl; dh.y += size;

	// --

	size_t val, vbl, vcl, vdl, vah, vbh, vch, vdh;

	// --

	if (flags & (BTMR_CF_MY | BTMR_CF_MX | BTMR_CF_MZ))
		val = btmr_genVtx(&al);
	if (flags & (BTMR_CF_MY | BTMR_CF_PX | BTMR_CF_MZ))
		vbl = btmr_genVtx(&bl);
	if (flags & (BTMR_CF_MY | BTMR_CF_MX | BTMR_CF_PZ))
		vcl = btmr_genVtx(&cl);
	if (flags & (BTMR_CF_MY | BTMR_CF_PX | BTMR_CF_PZ))
		vdl = btmr_genVtx(&dl);

	if (flags & (BTMR_CF_PY | BTMR_CF_MX | BTMR_CF_MZ))
		vah = btmr_genVtx(&ah);
	if (flags & (BTMR_CF_PY | BTMR_CF_PX | BTMR_CF_MZ))
		vbh = btmr_genVtx(&bh);
	if (flags & (BTMR_CF_PY | BTMR_CF_MX | BTMR_CF_PZ))
		vch = btmr_genVtx(&ch);
	if (flags & (BTMR_CF_PY | BTMR_CF_PX | BTMR_CF_PZ))
		vdh = btmr_genVtx(&dh);

	// --

	if (flags & BTMR_CF_MY)
		printf(BTMR_QUAD_FMT, val, vcl, vdl, vbl);
	if (flags & BTMR_CF_PY)
		printf(BTMR_QUAD_FMT, vah, vbh, vdh, vch);

	if (flags & BTMR_CF_PX)
		printf(BTMR_QUAD_FMT, vbh, vbl, vdl, vdh);
	if (flags & BTMR_CF_MX)
		printf(BTMR_QUAD_FMT, vah, vch, vcl, val);

	if (flags & BTMR_CF_PZ)
		printf(BTMR_QUAD_FMT, vch, vdh, vdl, vcl);
	if (flags & BTMR_CF_MZ)
		printf(BTMR_QUAD_FMT, vah, val, vbl, vbh);
}

void btmr_genMenger(const btmr_v3_t * pos, btmr_coord_t size, int flags, size_t depth) {
	if (depth == 0) {
		btmr_genCube(pos, size, flags);
	} else {
		btmr_coord_t sizePart = size / 3;
		for (int x = 0; x < 3; x++) {
			int tmpXFlags = 0;
			if (x == 0)
				tmpXFlags |= flags & BTMR_CF_MX;
			if (x == 2)
				tmpXFlags |= flags & BTMR_CF_PX;
			for (int y = 0; y < 3; y++) {
				int tmpYFlags = tmpXFlags;
				if (y == 0)
					tmpYFlags |= flags & BTMR_CF_MY;
				if (y == 2)
					tmpYFlags |= flags & BTMR_CF_PY;
				for (int z = 0; z < 3; z++) {
					int tmpZFlags = tmpYFlags;
					if (z == 0)
						tmpZFlags |= flags & BTMR_CF_MZ;
					if (z == 2)
						tmpZFlags |= flags & BTMR_CF_PZ;
					// now to create holes
					if ((x == 1) && (z == 1))
						continue;
					if ((y == 1) && (z == 1))
						continue;
					if ((x == 1) && (y == 1))
						continue;
					// and interior sections
					if ((x == 1) || (z == 1)) {
						if (y == 0)
							tmpZFlags |= BTMR_CF_PY;
						if (y == 2)
							tmpZFlags |= BTMR_CF_MY;
					}
					if ((y == 1) || (z == 1)) {
						if (x == 0)
							tmpZFlags |= BTMR_CF_PX;
						if (x == 2)
							tmpZFlags |= BTMR_CF_MX;
					}
					if ((x == 1) || (y == 1)) {
						if (z == 0)
							tmpZFlags |= BTMR_CF_PZ;
						if (z == 2)
							tmpZFlags |= BTMR_CF_MZ;
					}
					// finally, build it
					btmr_v3_t tmp = *pos;
					tmp.x += sizePart * x;
					tmp.y += sizePart * y;
					tmp.z += sizePart * z;
					btmr_genMenger(&tmp, sizePart, tmpZFlags, depth - 1);
				}
			}
		}
	}
}

int main(int argc, char ** argv) {
	if (argc != 2) {
		puts("btmr_gen_menger N");
		return 1;
	}
	size_t depth = atol(argv[1]);
	printf("# cube of depth %lu...\n", depth);
	btmr_v3_t origin = {};
	btmr_coord_t size = 1;
	// for (size_t i = 1; i <= depth; i++)
	//	size += size * 2;
	btmr_genMenger(&origin, size, BTMR_CF_FF, depth);
	return 0;
}
