#pragma once

// Opens a file for reading and maps it into memory.
// No, there's no way of *unmapping* the file.
void * btmr_mapDumb(const char * fn);

// Opaque type.
#ifndef btmr_mapping_t
#define btmr_mapping_t void *
#endif

// Opens a file for reading or reading/writing and maps it into memory.
// The file may be unmapped.
btmr_mapping_t btmr_mappingOpen(const char * fn, int rw);

// Gets the mapped location of the file.
void * btmr_mappingGetMemory(btmr_mapping_t map);

// Gets the size of the file.
size_t btmr_mappingGetSize(btmr_mapping_t map);

// Resizes the file (padding with zeroes).
// This may cause the mapping to be moved - the new location is returned.
// If NULL is returned, resizing the file failed and the mapping was closed as a result.
void * btmr_mappingSetSize(btmr_mapping_t map, size_t newSize);

void btmr_mappingClose(btmr_mapping_t map);

// -- Fixups --

typedef struct {
	void * parent;
	// NOTE: This really REALLY must be void ** - the pointer maths assumes applying * results in a "void *"
	void ** target;
} btmr_fixup_t;

void btmr_fixupAdd(btmr_mapping_t map, btmr_fixup_t * add, void ** fixup);
void btmr_fixupRm(btmr_mapping_t map, void ** fixup);

#define BTMR_FIXUP_ENTER(map, var) {btmr_fixupAdd(map, alloca(sizeof(btmr_fixup_t)), (void **) &var);}
#define BTMR_FIXUP_LEAVE(map, var) {btmr_fixupRm(map, (void **) &var);}

// -- Alloc --

// Allocates file memory. Note that allocations are linear and don't recycle.
void * btmr_mappingAlloc(btmr_mapping_t map, size_t len);

