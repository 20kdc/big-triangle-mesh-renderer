#include "btmr_format.h"
#include "btmr_io.h"
#include <stdio.h>
#include <string.h>

// theNode.tris[0] and theNode.triCounts[0] represent the state of the file as it's being worked on
btmr_node_t theNode;
// tracks 'v' command count (used for negative references)
size_t verticesWritten = 0;

int enableTriangleFlip = 0;
int traceWordSM = 0;

typedef struct {
	char * text;
	size_t size;
	// malloc'd buffer for this word - NULL if no word has been loaded yet
	char * word;
	// If this word is at the end-of-line
	int wordEOL;
	// If this word is at the end-of-file
	int wordEOF;
	const char * wordLine;
	size_t lineCount;
} btmr_textstream_t;

void btmr_nextWord(btmr_textstream_t * obj) {
	// clean up old...
	free(obj->word);
	if (obj->wordEOL) {
		obj->lineCount++;
		obj->wordLine = obj->text;
	}
	// setup new
	obj->word = malloc(1);
	obj->word[0] = 0;
	size_t wordLen = 0;
	obj->wordEOL = 0;
	obj->wordEOF = 0;
	int inStoppingState = 0;
	while (obj->size) {
		char ch = *obj->text;
		if (traceWordSM)
			printf("%c @ %i %i %s\n", ch, inStoppingState, (int) wordLen, obj->word);
		obj->text++;
		obj->size--;
		if (ch == 13) {
			// nope, this doesn't exist, shush
		} else if (ch == 10) {
			// EOL, stop here
			obj->wordEOL = 1;
			return;
		} else if (ch <= 32) {
			// whitespace, if we've started a word, activate stopping state
			if (wordLen)
				inStoppingState = 1;
		} else if (inStoppingState) {
			// hit another char and we're done with the word, go back 1
			obj->text--;
			obj->size++;
			return;
		} else {
			// add char to word
			obj->word[wordLen++] = ch;
			obj->word = realloc(obj->word, wordLen + 1);
			obj->word[wordLen] = 0;
		}
	}
	obj->wordEOL = 1;
	obj->wordEOF = 1;
}

void btmr_reportImpError(const char * err, btmr_textstream_t * obj) {
	printf("%s: '%s' at line %lu (%i)\n", err, obj->word, obj->lineCount, (int) (obj->text - obj->wordLine));
	const char * wl = obj->wordLine;
	while (*wl != 10) {
		putchar(*wl);
		wl++;
	}
	putchar(10);
	exit(1);
}

size_t btmr_nextVRef(btmr_textstream_t * obj) {
	btmr_nextWord(obj);
	char * xt = obj->word;
	while (*xt) {
		if (*xt == '/') {
			*xt = 0;
			break;
		}
		xt++;
	}
	long ref = atol(obj->word);
	size_t vi;
	if (ref > 0) {
		vi = ((size_t) ref - 1);
	} else if (ref < 0) {
		// this is NOT perfect support for backreferences, but it makes rungholt work
		vi = (size_t) (ref + verticesWritten);
	} else {
		btmr_reportImpError("vertex reference of 0 was found", obj);
	}
	return (vi * sizeof(btmr_vertex_t)) + sizeof(theNode);
}

void btmr_importVertices(btmr_textstream_t * obj, FILE * dst) {
	while (!obj->wordEOF) {
		// begin line
		btmr_nextWord(obj);
		if (!strcmp(obj->word, "v")) {
			// vertex
			btmr_vertex_t vtx;
			btmr_nextWord(obj);
			vtx.pos.x = atof(obj->word);
			btmr_nextWord(obj);
			vtx.pos.y = atof(obj->word);
			btmr_nextWord(obj);
			vtx.pos.z = atof(obj->word);
			// write it & update triBase
			fwrite(&vtx, sizeof(vtx), 1, dst);
			theNode.tris[0] += sizeof(vtx);
			verticesWritten++;
		}
		// finish line
		while (!obj->wordEOL)
			btmr_nextWord(obj);
	}
}
void btmr_swapTriangle(btmr_triangle_t * tri) {
	size_t tmp = tri->b;
	tri->b = tri->c;
	tri->c = tmp;
}
void btmr_importTriangles(btmr_textstream_t * obj, FILE * dst) {
	while (!obj->wordEOF) {
		// begin line
		btmr_nextWord(obj);
		if (!strcmp(obj->word, "f")) {
			// face ; this gets complicated very quickly...
			btmr_triangle_t tri;
			if (obj->wordEOL) {
				puts("missing face startup vertex A");
				exit(1);
			}
			tri.a = btmr_nextVRef(obj);
			if (obj->wordEOL) {
				puts("missing face startup vertex B");
				exit(1);
			}
			tri.b = btmr_nextVRef(obj);
			// finish line the fun way
			while (!obj->wordEOL) {
				// setup this triangle
				tri.c = btmr_nextVRef(obj);
				tri.next = theNode.tris[0] + ((theNode.triCounts[0] + 1) * sizeof(btmr_triangle_t));
				// swap direction if asked
				if (enableTriangleFlip)
					btmr_swapTriangle(&tri);
				// write
				fwrite(&tri, sizeof(tri), 1, dst);
				theNode.triCounts[0]++;
				// swap direction back if asked
				if (enableTriangleFlip)
					btmr_swapTriangle(&tri);
				// swap around
				tri.b = tri.c;
			}
		} else {
			// finish line
			while (!obj->wordEOL)
				btmr_nextWord(obj);
		}
	}
}

int main(int argc, char ** argv) {
	int unrecognized = 0;
	for (int i = 3; i < argc; i++) {
		const char * arg = argv[i];
		if (!strcmp(arg, "--triFlip")) {
			enableTriangleFlip = 1;
		} else {
			unrecognized = 1;
		}
	}
	if (unrecognized || (argc < 3)) {
		puts("btmr_import OBJFILE BTMRFILE [--triFlip]");
		return 1;
	}
	// ?
	btmr_mapping_t map = btmr_mappingOpen(argv[1], 0);
	if (!map) {
		puts("failed to open obj file");
		return 1;
	}
	btmr_textstream_t obj;
	obj.text = (char *) btmr_mappingGetMemory(map);
	obj.size = btmr_mappingGetSize(map);
	obj.word = NULL;
	obj.wordEOL = 0;
	obj.wordEOF = 0;
	obj.wordLine = obj.text;
	obj.lineCount = 1;
	FILE * dst = fopen(argv[2], "wb");
	if (!dst) {
		puts("failed to open target file");
		btmr_mappingClose(map);
		return 1;
	}
	// alright, do the thing
	// write node header (blank w/ offset)
	fwrite(&theNode, sizeof(theNode), 1, dst);
	// set triBase to vertexBase
	theNode.tris[0] = sizeof(theNode);
	btmr_textstream_t objSub = obj;
	btmr_importVertices(&objSub, dst);
	free(objSub.word);
	objSub = obj;
	btmr_importTriangles(&objSub, dst);
	free(objSub.word);
	// rewrite node header
	rewind(dst);
	fwrite(&theNode, sizeof(theNode), 1, dst);
	// done
	fclose(dst);
	btmr_mappingClose(map);
	return 0;
}
