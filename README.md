# Big Triangle Mesh Renderer

The Big Triangle Mesh Renderer is a set of programs:

+ `btmr_import OBJFILE BTMRFILE`, converts OBJ files with triangles and quads to the format used by BTMR

+ `btmr_bsp BTMRFILE BUDGET`, splits a BTMR file via a sort of BSP into different areas (`BUDGET` is the maximum amount of triangles for each node)

+ `btmr_nbh BTMRFILE`, connects neighbours

+ `btmr_genMenger N` writes a menger sponge OBJ file of complexity N to stdout

+ `btmr BTMRFILE...`, explores a list of BTMR files

When multiple files are involved, they are drawn from first to last with depth cleared between them.

Usually you'd pass >1 file when each file represents a different level of detail (from lowest to highest)

WARNING: BTMR files are NOT PORTABLE AND NOT STABLE! Use OBJ files or some compacted version thereof instead for storage.

BTMR has successfully been tested on a file of 17692449 quads (converted to 35384898 triangles) containing a ~4k x 4k heightmap, so as far as I'm concerned it works well enough.

To be clear, this was released because someone might have use for it.

## Example

```
make NO_MINGW=1
mkdir data
bin/btmr_genMenger 3 > data/cube3.obj # ~2MB
bin/btmr_import data/cube3.obj data/cube3.btmr
bin/btmr_genMenger 5 > data/cube5.obj # ~800MB
bin/btmr_import data/cube5.obj data/cube5.btmr
bin/btmr_bsp data/cube5.btmr 65536 # increase if you think your GPU can take it (I use 131072 with an AMD REDWOOD card)
bin/btmr_nbh data/cube5.btmr
bin/btmr data/cube3.btmr data/cube5.btmr
```

## Controls

+ WASD - fly
+ QE - fly speed
+ IJKL - pan
+ TFGH - set yaw
+ UPO - set pitch

## Dependencies

1. In practice you'll want to be using a 64-bit operating system because the entire database file is mapped into virtual memory
2. Linux or Windows, maybe Mac OS X but untested, and I'm not certain on the stability of the Windows port
3. SDL2
4. OpenGL 1.5 (yes, 1.5, desktop. this requirement is NOT changing, ever)

The program should be small enough that you can fix any issues that crop up.

## License

CC0 (see COPYING.txt)
