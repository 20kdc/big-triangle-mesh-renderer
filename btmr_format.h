/*
 * FORMAT DESIGN:
 * Format uses a binary tree, balanced based on triangle count.
 * No occlusion but heavy LOD.
 * Memory-map the whole file (what good is a 64-bit address space if you don't use it)
 * Relevant leaf of tree is tracked at user level.
 * Start of file = root of tree.
 * Offset of 0 means null otherwise.
 */

#include <stdint.h>
#include <stdlib.h>

#pragma once

typedef float btmr_coord_t;

typedef struct {
	btmr_coord_t x;
	btmr_coord_t y;
	btmr_coord_t z;
} btmr_v3_t;

typedef struct {
	btmr_v3_t pos;
} btmr_vertex_t;

// A triangle is only "owned" by one node (that node for which this triangle is part of model 0).
// A triangle may be *referred to* by other nodes using the other models, though.
// Where this causes issues, wipe the other models.
typedef struct {
	// A/B/C point vertex indices
	size_t a, b, c;
	// next triangle offset. NOTE THAT THIS MAY BE INVALID. ONLY triCount IS DEFINITIVE.
	size_t next;
} btmr_triangle_t;

#define BTMR_MAX_MODELS 32

// Present at offset 0 of the file
typedef struct {
	// Split plane axis (X/Y/Z) ('above' is always +)
	int planeAxis;
	// Split plane position
	btmr_coord_t planePos;
	// offset of btmr_node_t
	// either both of these are NULL or neither of them are (so expect just 'a' to be checked)
	// if they're both NULL, this is a leaf
	// 'a' is above the plane, 'b' below it
	size_t a, b;
	// Models: model 0 is the "primary" model, while others are cross-references.
	// These cross-references are specifically for btmr_nbh to generate.
	// So the BSP splitter nukes all cross-references because it modifies the primary model in-place.
	// offsets of first triangles of models
	size_t tris[BTMR_MAX_MODELS];
	// lengths of linked lists of triangles
	size_t triCounts[BTMR_MAX_MODELS];
} btmr_node_t;

#define BTMR_AT(type, base, offset) ((type *)((base) + (offset)))
#define BTMR_OFS(base, obj) ((size_t) (((void *)(obj)) - (base)))

